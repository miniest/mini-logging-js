/* eslint no-use-before-define:off */

const LVL_TRACE = 100;
const LVL_DEBUG = 200;
const LVL_INFO = 300;
const LVL_WARN = 400;
const LVL_ERROR = 500;
const LVL_FATAL = 600;
const LVL_NONE = 9999;
/**
 * Convert logging level to value.
 *
 * @param {string} level - level to convert
 *
 * @private
 *
 * @return {number} - numerical relresentation of level
 */
function lvl2number(level) {
    switch (level) {
        case Logger.TRACE:
            return LVL_TRACE;
        case Logger.DEBUG:
            return LVL_DEBUG;
        case Logger.INFO:
            return LVL_INFO;
        case Logger.WARN:
            return LVL_WARN;
        case Logger.ERROR:
            return LVL_ERROR;
        case Logger.FATAL:
            return LVL_FATAL;
        case Logger.NONE:
            return LVL_NONE;
        default:
            return LVL_NONE;
    }
}

function number2lvl(num) {
    switch (num) {
        case LVL_TRACE:
            return Logger.TRACE;
        case LVL_DEBUG:
            return Logger.DEBUG;
        case LVL_INFO:
            return Logger.INFO;
        case LVL_WARN:
            return Logger.WARN;
        case LVL_ERROR:
            return Logger.ERROR;
        case LVL_FATAL:
            return Logger.FATAL;
        default:
            return Logger.NONE;
    }
}

/**
 * Individual logger representation
 */
export class Logger {
    static get TRACE() {
        return "TRACE";
    }

    static get DEBUG() {
        return "DEBUG";
    }

    static get INFO() {
        return "INFO";
    }

    static get WARN() {
        return "WARN";
    }

    static get ERROR() {
        return "ERROR";
    }

    static get FATAL() {
        return "FATAL";
    }

    static get NONE() {
        return "NONE";
    }

    /**
     * Create logger instance
     *
     * @param {string} [name = "unnamed"] - name of the logger
     * @param {string} [verbosity = Logger.INFO] - voller level as string
     * (use logger level constants)
     * @param {function} [writter = NOOP function] - writter to use
     */
    constructor(
        name = "default",
        verbosity = Logger.INFO,
        writter = function noop() {},
    ) {
        this._name = name;
        this._verbosity = lvl2number(verbosity);
        this._writter = writter;
    }

    /**
     * Internal log method
     *
     * @param {string} lvl - log level
     * @param {...object} msg - message compatible with writter format
     *
     * @private
     *
     * @return {Logger} this
     */
    log(lvl, ...msg) {
        if (msg.length > 0) {
            if (typeof msg[0] === "string") {
                /* eslint-disable-next-line no-param-reassign */
                msg[0] = `lvl=${lvl} lgr=${this._name} ${msg[0]}`;
            } else {
                msg.unshift(`lvl=${lvl} lgr=${this._name}`);
            }

            this._writter(...msg);
        }
    }

    /**
     * Log message on TRACE level
     *
     * @param {...object} msg - message compativle with writter format
     *
     * @return {Logger} this
     */
    trace(...msg) {
        if (LVL_TRACE >= this._verbosity) {
            this.log(Logger.TRACE, ...msg);
        }

        return this;
    }

    /**
     * Bound variant of trace method
     *
     * @return {function} function bound to this instance
     */
    get traceFn() {
        return this.trace.bind(this);
    }

    /**
     * Log message on DEBUG level
     *
     * @param {...object} msg - message compativle with writter format
     *
     * @return {Logger} this
     */
    debug(...msg) {
        if (LVL_DEBUG >= this._verbosity) {
            this.log(Logger.DEBUG, ...msg);
        }

        return this;
    }

    /**
     * Bound variant of debug method
     *
     * @return {function} function bound to this instance
     */
    get debugFn() {
        return this.debug.bind(this);
    }

    /**
     * Log message on INFO level
     *
     * @param {...object} msg - message compativle with writter format
     *
     * @return {Logger} this
     */
    info(...msg) {
        if (LVL_INFO >= this._verbosity) {
            this.log(Logger.INFO, ...msg);
        }

        return this;
    }

    /**
     * Bound variant of info method
     *
     * @return {function} function bound to this instance
     */
    get infoFn() {
        return this.info.bind(this);
    }

    /**
     * Log message on WARN level
     *
     * @param {...object} msg - message compativle with writter format
     *
     * @return {Logger} this
     */
    warn(...msg) {
        if (LVL_WARN >= this._verbosity) {
            this.log(Logger.WARN, ...msg);
        }

        return this;
    }

    /**
     * Bound variant of warn method
     *
     * @return {function} function bound to this instance
     */
    get warnFn() {
        return this.warn.bind(this);
    }

    /**
     * Log message on ERROR level
     *
     * @param {...object} msg - message compativle with writter format
     *
     * @return {Logger} this
     */
    error(...msg) {
        if (LVL_ERROR >= this._verbosity) {
            this.log(Logger.ERROR, ...msg);
        }

        return this;
    }

    /**
     * Bound variant of error method
     *
     * @return {function} function bound to this instance
     */
    get errorFn() {
        return this.error.bind(this);
    }

    /**
     * Log message on FATAL level
     *
     * @param {...object} msg - message compativle with writter format
     *
     * @return {Logger} this
     */
    fatal(...msg) {
        if (LVL_FATAL >= this._verbosity) {
            // TODO may be system.exit???
            this.log(Logger.FATAL, ...msg);
        }

        return this;
    }

    /**
     * Bound variant of fatal method
     *
     * @return {function} function bound to this instance
     */
    get fatalFn() {
        return this.fatal.bind(this);
    }

    get verbosity() {
        return number2lvl(this._verbosity);
    }

    /**
     * Set logger verbosity
     *
     * @param {string} [level = Logger.NONE] - level to set
     */
    set verbosity(level = Logger.NONE) {
        this._verbosity = lvl2number(level);
        return this;
    }
}
