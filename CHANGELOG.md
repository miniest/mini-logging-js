# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/miniest/mini-logging-js/compare/v1.0.0...v1.1.0) (2020-06-05)


### Features

* getLoggerByPath ([a608dd8](https://gitlab.com/miniest/mini-logging-js/commit/a608dd8b65a9fb17cda96bada0d1e771696330f9))

<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/miniest/mini-logging-js/compare/v0.0.4...v1.0.0) (2019-12-02)


### Features

* overhaul of logging, major version preparation ([d2edf35](https://gitlab.com/miniest/mini-logging-js/commit/d2edf35))


### BREAKING CHANGES

* logging API and usage changed significantly
