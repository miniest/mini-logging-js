import { Logger } from "./Logger.js";

/**
 * Logging manager. Provides utilities to get and manage individual loggers.
 */
export class Logging {
    constructor() {
        /* eslint-disable-next-line no-console */
        this._writter = console.log;
        this._loggers = new Map();
        this._defaultVerbosity = Logger.INFO;
    }

    get writter() {
        return this._writter;
    }

    set writter(writter) {
        this._writter = writter;
    }

    get defaultVerbosity() {
        return this._defaultVerbosity;
    }

    set defaultVerbosity(level = Logger.NONE) {
        this._defaultVerbosity = level;
    }

    /**
     * Get instance of the logger by its name. If there is no instance with requested name,
     * new instance is created.
     *
     * @param {any} name name of the logger
     *
     * @return {Logger} logger
     */
    getLogger(name) {
        if (this._loggers.has(name)) {
            return this._loggers.get(name);
        }
        const newLogger = new Logger(
            name,
            this._defaultVerbosity,
            this._writter,
        );
        this._loggers.set(name, newLogger);

        return newLogger;
    }

    /**
     * Get logger with name parsed from the path. Name is calculated relative to the
     * current directory. It path ends with .js extension it is stripped.
     *
     * @example
     * let log = Logging.getLoggerByPath(import.meta.url);
     *
     * @param {string} path - path to be parsed
     *
     * @return {Logger} logger
     */
    getLoggerByPath(path) {
        const url = new URL(path);

        let name = url.pathname;

        if (url.protocol === "file:") {
            // we are in nodejs probably
            // but anyway we cannot use path module here

            if (name.startsWith(process.cwd())) {
                // +1 removes initial slash
                name = name.substr(process.cwd().length + 1);
            }

            // remove js extension if present
            if (name.substr(-3) === ".js") {
                name = name.substr(0, name.length - 3);
            }
        } else {
            throw new Error("Unsupported protocol");
        }

        return this.getLogger(name);
    }

    /**
     * Sets verbosity of logging
     *
     * @param {string} [level=Logger.INFO] - level of logging verbosity
     * @param {string|Logger} logger - name of logger or instance of logger to set verbosity for
     *
     * @return this
     */
    setVerbosity(level = Logger.INFO, logger) {
        if (logger) {
            // set verbosity for specified logger
            if (logger instanceof Logger) {
                /* eslint-disable-next-line no-param-reassign */
                logger.verbosity = level;
            } else {
                this.getLogger(logger).verbosity = level;
            }
        } else {
            // no logger specified, set level to all loggers
            /* eslint-disable-next-line no-param-reassign */
            this._loggers.forEach((l) => { l.verbosity = level; });
        }
    }
}
