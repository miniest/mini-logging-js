/* eslint-env mocha */

import chai from "chai";
import { Logging } from "../../Logging.js";
import { Logger } from "../../Logger.js";


const { assert } = chai;

describe("Logging", () => {
    it("instantiate", async () => {
        const logging = new Logging();

        assert.isDefined(logging);
    });

    it("should return same instance of logger with same name", async () => {
        const logging = new Logging();

        const l1 = logging.getLogger("test");
        const l2 = logging.getLogger("test");

        assert.equal(l1, l2);
    });

    it("should allow to set writter and default verbosity", async () => {
        const logging = new Logging();

        const writterFn = function() {};

        logging.writter = writterFn;
        logging.defaultVerbosity = Logger.ERROR;

        assert.equal(logging.writter, writterFn);
        assert.equal(logging.defaultVerbosity, Logger.ERROR);

        const log = logging.getLogger("TEST");

        assert.equal(log._writter, writterFn);
        assert.equal(log.verbosity, Logger.ERROR);

        logging.defaultVerbosity = Logger.TRACE;

        const log2 = logging.getLogger("TEST2");

        assert.equal(log2.verbosity, Logger.TRACE);
    });

    it("should set verbosity to individual and all loggers", async () => {
        const logging = new Logging();

        const writterFn = function() {};

        logging.writter = writterFn;
        logging.defaultVerbosity = Logger.ERROR;

        const log = logging.getLogger("TEST");
        const log2 = logging.getLogger("TEST2");

        logging.setVerbosity(Logger.TRACE, log);

        assert.equal(log.verbosity, Logger.TRACE);
        assert.equal(log2.verbosity, Logger.ERROR);

        logging.setVerbosity(Logger.DEBUG, "TEST2");

        assert.equal(log.verbosity, Logger.TRACE);
        assert.equal(log2.verbosity, Logger.DEBUG);

        logging.setVerbosity(Logger.WARN);

        assert.equal(log.verbosity, Logger.WARN);
        assert.equal(log2.verbosity, Logger.WARN);

        logging.setVerbosity();

        assert.equal(log.verbosity, Logger.INFO);
        assert.equal(log2.verbosity, Logger.INFO);
    });

    it("should get name from path", async () => {
        const logging = new Logging();

        const log = logging.getLoggerByPath(`file://${process.cwd()}/test1.js`);

        assert.equal(log._name, "test1");
    });
});
