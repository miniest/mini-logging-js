/* eslint-env mocha */

import chai from "chai";
import { Logger } from "../../Logger.js";

const { assert } = chai;

describe("Logger", () => {
    it("should instantiate", async () => {
        const log = new Logger();

        assert.instanceOf(log, Logger);
    });

    it("should log by verbosity", async () => {
        const log = new Logger("Test2 logger");

        let result = null;
        log._writter = function(...data) {
            result = data;
        };

        // TRACE
        result = null;
        log.verbosity = Logger.TRACE;
        log.trace("aaa");
        assert.isArray(result);
        result = null;
        log.verbosity = Logger.DEBUG;
        log.trace("bbb");
        assert.isNull(result);

        // DEBUG
        result = null;
        log.verbosity = Logger.DEBUG;
        log.debug("aaa");
        assert.isArray(result);
        result = null;
        log.verbosity = Logger.INFO;
        log.debug("bbb");
        assert.isNull(result);

        // INFO
        result = null;
        log.verbosity = Logger.INFO;
        log.info("aaa");
        assert.isArray(result);
        result = null;
        log.verbosity = Logger.WARN;
        log.info("bbb");
        assert.isNull(result);

        // WARN
        result = null;
        log.verbosity = Logger.WARN;
        log.warn("aaa");
        assert.isArray(result);
        result = null;
        log.verbosity = Logger.ERROR;
        log.warn("bbb");
        assert.isNull(result);

        // ERROR
        result = null;
        log.verbosity = Logger.ERROR;
        log.error("aaa");
        assert.isArray(result);
        result = null;
        log.verbosity = Logger.FATAL;
        log.error("bbb");
        assert.isNull(result);

        // FATAL
        result = null;
        log.verbosity = Logger.FATAL;
        log.fatal("aaa");
        assert.isArray(result);
        result = null;
        log.verbosity = Logger.NONE;
        log.fatal("bbb");
        assert.isNull(result);

        // Invalid verbosity
        result = null;
        log.verbosity = Logger.TRACE;
        log.verbosity = "UNKNOWN";
        log.trace("aaa");
        assert.isNull(result);
        log.verbosity = 2;
        log.trace("aaa");
        assert.isNull(result);

        result = "untouched";
        log.verbosity = Logger.TRACE;
        log.trace();
        assert.equal(result, "untouched");
    });

    it("should log various formats", async () => {
        const log = new Logger("Test3");

        let result = null;
        log._writter = function(...data) {
            result = data;
        };

        log.verbosity = Logger.TRACE;
        log.trace("TEXT");
        log.trace(1);
        assert.isArray(result);
    });

    it("should provide bound functions", async () => {
        const log = new Logger("Test4");

        let result = null;
        log._writter = function(...data) {
            result = data;
        };

        log.verbosity = Logger.TRACE;
        log.traceFn(-1);
        assert.include(result, -1);
        log.debugFn(-2);
        assert.include(result, -2);
        log.infoFn(-3);
        assert.include(result, -3);
        log.warnFn(-4);
        assert.include(result, -4);
        log.errorFn(-5);
        assert.include(result, -5);
        log.fatalFn(-6);
        assert.include(result, -6);
    });
});
